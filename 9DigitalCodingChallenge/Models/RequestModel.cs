﻿using System.Collections.Generic;

namespace _9DigitalCodingChallenge.Models
{
	public class RequestModel
	{
		public IList<Payload> Payload { get; set; }
		public int Skip { get; set; }
		public int Take { get; set; }
		public int TotalRecords { get; set; }
	}
}