﻿using _9DigitalCodingChallenge.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace _9DigitalCodingChallenge.Controllers
{
	public class HomeController : Controller
	{

		//Validate JSON
		JSchema requestSchema = JSchema.Parse(@"{  
				'type': 'object',  
				'properties': {  
					'payload': {'type':'array'},  
					'skip':{'type':'integer'},
					'take':{'type':'integer'},
					'totalRecords':{'type':'integer'}
				}  
			}");

		JSchema payloadSchema = JSchema.Parse(@"{  
					'type': 'object',  
					'properties': {  
						'country': {'type':'string'},  
						'description':{'type':'string'},
						'drm':{'type':'string'},
						'episodeCount':{'type':'integer'},
						'image':{'type':'string'},
						'language':{'type':'string'},
						'nextEpisode':{'type':'object'},
						'primaryColour':{'type':'string'},
						'seasons':{'type':'array'},
						'slug':{'type':'string'},
						'title':{'type':'string'},
						'tvChannel':{'type':'string'}
					}  
				}");

		#region Action

		public JsonResult Index()
		{
			var url = "http://codingchallenge.nine.com.au/sample_request.json";
			var jsonData = getWebContent(url);
			return readJsonString(jsonData);
		}

		#endregion

		#region Helper

		private string getWebContent(string url)
		{
			Uri uri = new Uri(url);
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
			request.Method = WebRequestMethods.Http.Get;
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());
			string output = reader.ReadToEnd();
			response.Close();
			return output;
		}

		private JsonResult readJsonString(string jsonData)
		{
			bool valid = false;
			var settings = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
				MissingMemberHandling = MissingMemberHandling.Ignore
			};

			JObject requestObject = JObject.Parse(jsonData);
			valid = requestObject.IsValid(requestSchema);
			if (!valid)
				return Error();

			try
			{
			//	var model = JsonConvert.DeserializeObject<RequestModel>(jsonData, settings);
				var model = JsonConvert.DeserializeObject<RequestModel>(jsonData);
				List<ResponseModel> responseList = new List<ResponseModel>();

				foreach (var element in model.Payload)
				{
					JObject payloadObject = JObject.Parse(JsonConvert.SerializeObject(element));
				  valid = payloadObject.IsValid(payloadSchema);
					if (!valid)
						return Error();

					if (element.DRM && element.EpisodeCount > 0)
					{
						ResponseModel responseModel = new ResponseModel();
						responseModel.Image = element.Image.ShowImage;
						responseModel.Slug = element.Slug;
						responseModel.Title = element.Title;
						responseList.Add(responseModel);
					}
				}

				ResponseModel[] response = responseList.ToArray();
				string jsonResponse = JsonConvert.SerializeObject(response, Formatting.Indented);

				return Ok(jsonResponse);
			}
			catch
			{
				return Error();
			}
		}

		private JsonResult Ok(string jsonResponse)
		{
			var result = new
			{
				//statusCode = 200,
				//status = "OK",
				response = jsonResponse,
			};
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		private JsonResult Error()
		{
			var result =new{
				//statusCode = 400,
				status = "400 Bad Request",
				response = "Error",
				error = "Could not decode request: JSON parsing failed"
			};

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		#endregion

	}
}