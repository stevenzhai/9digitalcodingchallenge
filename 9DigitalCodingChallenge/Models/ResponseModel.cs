﻿namespace _9DigitalCodingChallenge.Models
{
	public class ResponseModel
	{
		public string Image { get; set; }
		public string Slug { get; set; }
		public string Title { get; set; }
	}
}